//
// Created by kamil on 4/7/21.
//

#include "NodeMap.h"

NodeMap::NodeMap(const std::vector<Node> &nodes, int width, int height) : mapNodes(nodes), mapWidth(width), mapHeight(height){

}

std::vector<Node *> NodeMap::neighboursOfNode(const Node *node) const {
    std::vector<Node *> neighbours;
    auto nodePosition = node->position;
    std::vector<astar::Point2i> directions = {{1, 0}, {-1 , 0}, {0, 1}, {0, -1}, {1, 1}, {-1, -1}, {1, -1}, {-1, 1}};

    for(const auto& direction : directions) {
        auto neighbourPosition = nodePosition + direction;
        if(isPositionValid(neighbourPosition) && !node->obstacle){
            neighbours.push_back(findNodeByPosition(neighbourPosition));
        }
    }
    return neighbours;
}

Node * NodeMap::findNodeByPosition(const astar::Point2i &position) const {
    int nodeIndex = position.x + mapWidth * position.y;
    return const_cast<Node *>(&mapNodes.at(nodeIndex));
}

bool NodeMap::isPositionValid(const astar::Point2i &position) const {
    auto x = position.x;
    auto y = position.y;
    return x >= 0 && x < mapWidth && y >= 0 && y < mapHeight;
}

