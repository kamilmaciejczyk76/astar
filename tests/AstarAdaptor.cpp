//
// Created by kamil on 4/7/21.
//

#include "AstarAdaptor.h"

std::optional<std::vector<astar::Point2i>>
AstarAdaptor::findPath(const std::vector<int> &intMap, int mapWidth, int mapHeight, astar::Point2i startPosition,
                       astar::Point2i destination) {

    NodeMap nodeMap = translateIntMapToNodeMap(intMap, mapWidth, mapHeight);

    auto path = astar::findPath<Node, NodeMap>(nodeMap, nodeMap.findNodeByPosition(startPosition), nodeMap.findNodeByPosition(destination));

    std::optional<std::vector<astar::Point2i>> translatedPath;
    std::vector<astar::Point2i> nonOptionalPath;

    if(!path.has_value()) return std::nullopt;
    for(const auto& point : path.value()){
        nonOptionalPath.emplace_back(point.position.x, point.position.y);
    }
    translatedPath.emplace(nonOptionalPath);
    return translatedPath;
}

NodeMap AstarAdaptor::translateIntMapToNodeMap(const std::vector<int> &intMap, int mapWidth, int mapHeight) {
    std::vector<Node> nodesOfMap;

    astar::Point2i nodePosition;

    for(int i = 0; i < intMap.size(); i++){
        nodePosition = {i % mapWidth, i / mapWidth };
        nodesOfMap.emplace_back(nodePosition, intMap[i]);
    }

    return NodeMap(nodesOfMap, mapWidth, mapHeight);
}