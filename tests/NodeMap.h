//
// Created by kamil on 4/7/21.
//

#ifndef QT_SFML_TEST_NODEMAP_H
#define QT_SFML_TEST_NODEMAP_H

#include <vector>
#include "Node.h"

class NodeMap {
public:
    explicit

    NodeMap(const std::vector<Node> &nodes, int width, int height);

    std::vector<Node *> neighboursOfNode(const Node *node) const;
    [[nodiscard]] Node * findNodeByPosition(const astar::Point2i &position) const;

private:
    [[nodiscard]] bool isPositionValid(const astar::Point2i &position) const;

    std::vector<Node> mapNodes;
    int mapWidth{0};
    int mapHeight{0};
};

#endif  //QT_SFML_TEST_NODEMAP_H
