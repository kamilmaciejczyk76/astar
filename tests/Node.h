//
// Created by kamil on 4/7/21.
//

#ifndef QT_SFML_TEST_NODE_H
#define QT_SFML_TEST_NODE_H

#include <Point2i.h>
#include <cstdint>

struct Node {
  explicit Node(const astar::Point2i &position, bool obstacle = false) : position(position), obstacle(obstacle) {}
  Node() = default;

  astar::Point2i position{0, 0};
  bool obstacle{false};
  const Node *parent{nullptr};
};

#endif  //QT_SFML_TEST_NODE_H
