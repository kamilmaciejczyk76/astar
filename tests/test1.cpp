//
// Created by kamil on 4/14/21.
//

#include "AstarAdaptor.h"
#include <gtest/gtest.h>


std::vector<int> map1 = {   0, 0, 0, 0, 0, 0,
                            0, 1, 1, 1, 1, 0,
                            0, 1, 0, 0, 1, 0,
                            0, 1, 0, 0, 1, 0,
                            0, 1, 1, 1, 1, 0,
                            0, 1, 0, 0, 0, 0};

int width = 6;
int height = 6;

GTEST_TEST(basic_test, path_right_upper_corner){
    std::vector<astar::Point2i> path1 = {{0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}};
    EXPECT_EQ(path1, AstarAdaptor::findPath(map1, width , height, {0,0}, {5, 0}).value());
}

GTEST_TEST(basic_test, path_left_lower_corner){
    std::vector<astar::Point2i> path2 = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}};
    EXPECT_EQ(path2, AstarAdaptor::findPath(map1, width , height, {0,0}, {0, 5}).value());
}

GTEST_TEST(basic_test, path_right_lower_corner){
    std::vector<astar::Point2i> path3 = {{0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 1}, {5, 2}, {5, 3}, {5, 4}};
    EXPECT_EQ(path3, AstarAdaptor::findPath(map1, width , height, {0,0}, {5, 5}).value());
}

GTEST_TEST(basic_test, no_path){
    EXPECT_EQ(std::nullopt, AstarAdaptor::findPath(map1, width , height, {0,0}, {3, 3}));
}

GTEST_TEST(basic_test, goal_sam_as_destination){
    EXPECT_EQ(std::nullopt, AstarAdaptor::findPath(map1, width , height, {0, 0}, {0, 0}));
}