//
// Created by kamil on 4/7/21.
//

#ifndef QT_SFML_TEST_ASTARADAPTOR_H
#define QT_SFML_TEST_ASTARADAPTOR_H

#include "NodeMap.h"
#include <Point2i.h>
#include <Astar.h>
#include <optional>

struct AstarAdaptor {
    AstarAdaptor() = default;

    static std::optional<std::vector<astar::Point2i>>
    findPath(const std::vector<int> &intMap, int mapWidth, int mapHeight, astar::Point2i startPosition, astar::Point2i destination);
    static NodeMap translateIntMapToNodeMap(const std::vector<int> &intMap, int mapWidth, int mapHeight);
};



#endif //QT_SFML_TEST_ASTARADAPTOR_H
