//
// Created by kamil on 4/13/21.
//

#ifndef ASTAR_POINT2I_H
#define ASTAR_POINT2I_H

namespace astar {
    struct Point2i {
        Point2i() = default;

        Point2i(int x, int y) : x(x), y(y) {};

        inline Point2i operator+(const Point2i &b) const {
            return Point2i(this->x + b.x, this->y + b.y);
        }

        inline bool operator==(const Point2i &b) const {
            return this->x == b.x && this->y == b.y;
        }

        inline Point2i &operator=(const Point2i &b) {
            if (this == &b)
                return *this;
            else {
                this->x = b.x;
                this->y = b.y;
                return *this;
            }
        }

        int x{0};
        int y{0};
    };
}
#endif //ASTAR_POINT2I_H
