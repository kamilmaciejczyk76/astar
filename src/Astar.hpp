//
// Created by kamil on 20.03.2021.
//

#include "Point2i.h"
#include <cmath>
#include <queue>
#include <unordered_map>

const inline auto MaxScore = INT16_MAX;

struct Score {
    float f{MaxScore};
    float g{MaxScore};
    bool visited{false};
};

template <typename T>
astar::Path<T> reconstructPathFromNode(const T *node) {
    astar::Path<T> path;
    while (node->parent != nullptr) {
        node = node->parent;
        path.insert(path.begin(), *node);
    }
    return path;
}

template <typename T>
float computeGCoefficient(const T *start, const T *destination) {
    std::vector<astar::Point2i> directions = {{1,  0},{-1, 0},{0,  1},{0,  -1}};
    for (const auto &direction: directions) {
        if (start->position == destination->position + direction) return 1;
    }
    return 1.41;  // sqrt(2)
}

template <typename T>
float computeHCoefficient(const T *start, const T *destination) {
    auto startPosition = start->position;
    auto destinationPosition = destination->position;

    return static_cast<float>(1.0f * sqrt(pow(startPosition.x - destinationPosition.x, 2) +
                                          pow(startPosition.y - destinationPosition.y, 2)));
}

template<typename T, typename V>
std::optional<astar::Path<T>> astar::findPath(const V &map, const T *startNode, const T *destinationNode) {


    if(startNode-> obstacle || destinationNode->obstacle || startNode == destinationNode) return std::nullopt;

    std::unordered_map<const T *, Score> nodesScores;

    auto compare = [&nodesScores](const  T * a, const T * b){
        return nodesScores[a].f > nodesScores[b].f ;
    };

    std::priority_queue<const T *, std::vector<const T *>, decltype(compare)> nodesToVisit(compare);

    auto f = computeHCoefficient(startNode, destinationNode);
    nodesScores[startNode] = {f, 0, true};
    nodesToVisit.push(startNode);

    const T *currentNode = nullptr;

    while (!nodesToVisit.empty()) {
        currentNode = nodesToVisit.top();
        nodesScores[currentNode].visited = true;

        nodesToVisit.pop();

        if (currentNode == destinationNode) return reconstructPathFromNode(currentNode);

        for (const auto &neighbour : map.neighboursOfNode(currentNode)) {

            if(neighbour->obstacle) continue;

            auto currentNodeGScore = nodesScores.contains(currentNode) ? nodesScores[currentNode].g : INT16_MAX;
            auto tentative_gScore = currentNodeGScore + computeGCoefficient(currentNode, neighbour);
            auto neighbourNodeGScore = nodesScores.contains(neighbour) ? nodesScores[neighbour].g : INT16_MAX;

            if (tentative_gScore < neighbourNodeGScore) {
                neighbour->parent = currentNode;
                f = tentative_gScore + computeHCoefficient(neighbour, destinationNode);
                nodesScores[neighbour] = {f, tentative_gScore};

                if (!nodesScores[neighbour].visited) {
                    nodesToVisit.push(neighbour);
                }
            }
        }
    }

    return std::nullopt;
}

