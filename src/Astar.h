//
// Created by kamil on 20.03.2021.
//

#ifndef QT_SFML_TEST_ASTAR_H
#define QT_SFML_TEST_ASTAR_H

#include <vector>
#include <optional>

namespace astar {
    template <typename T>
    using Path = typename std::vector<T>;
    template <typename T, typename V>
    std::optional<Path<T>> findPath(const V &map, const T *startNode, const T *destinationNode);
}

#include "Astar.hpp"

#endif  // QT_SFML_TEST_ASTAR_H
