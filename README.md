# A* search algorithm

Simple and efficient version of well-known algorithm that searches the shortest path.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

There's no required prerequisites.

### Installing

1. Copy this repo to your project. 
2. In top-level CMakeLists.txt add following line:

```
add_subdirectory(${PROJECT_SOURCE_DIR}/astar)

```
And after add_executable() type:

```
target_link_libraries(${PROJECT_NAME} astar)
```

## Using the library

You need to write yor own implementation of following structures:
* Node
* Map of nodes

Check /test directory to see example.
